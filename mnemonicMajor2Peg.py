import itertools
from PegSheet import PegSheet, PegType


ps = PegSheet("2pegSheet.txt")

while True:
    try:
        num = int(raw_input("Enter a number to convert: "))
        break
    except ValueError:
        print("Not an integer")
        continue

runningStr = ""
i = 0
# itertools.izip_longest is used such that if a number with an odd # of digits is used, its lattervalue is filled with an empty string
# otherwise using just zip it's simply skipped
for x, y in itertools.izip_longest(str(num)[::2], str(num)[1::2], fillvalue=''):
    numStr = x + y
    if i / 2 % 3 == 0:
        type_list = PegType.ADJECTIVE_TYPE
    if i / 2 % 3 == 1:
        type_list = PegType.NOUN_TYPE
    if i / 2 % 3 == 2:
        type_list = PegType.VERB_TYPE

    runningStr += ps.get_peg(numStr, type_list) + " "
    i = i + 2

print runningStr