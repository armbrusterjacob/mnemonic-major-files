import wordToMajor as wtm
import singularizeList as sl
import sys
import argparse

# Setup commandline args
parser = argparse.ArgumentParser()
parser.add_argument("in_file", help="filepath for the input")
parser.add_argument("-o", "--out_file", help="filepath for output, by default it's \'[name]_old.[ext]\'")
parser.add_argument("-p", "--peg_count", help="skips over all items but those with the selected type of peg",
    type=int, default=-1)
group = parser.add_mutually_exclusive_group()
group.add_argument("-s", "--singular", help="convert words to singular versions", action="store_true")
group.add_argument("-pl", "--plural", help="convert words to plural versions", action="store_true")
args = parser.parse_args()




filename = args.in_file
if args.out_file:
    new_filename = args.out_file
else:
    dot_pos = filename.rfind('.')
    new_filename = filename[:dot_pos] + "_out" + filename[dot_pos:]
    
peg_count = args.peg_count
word_list = wtm.fileToList(filename)


skip_list = []
if args.plural:
    word_list, skip_list = sl.listToPluralList(word_list)
elif args.singular:
    word_list, skip_list = sl.listToSingularList(word_list)

names_to_major = wtm.outputMajorDict(word_list, peg_count=peg_count)
# sort key really only works for words with less than 9 pegs, sorts based on peg count, then on the peg itself
wtm.outputMajortoFile(names_to_major, new_filename,
    skip_list,
    sort_key=lambda x: (10000000000 * len(x[1][1])) + int(x[1][1] if x[1][1] else -1)
)