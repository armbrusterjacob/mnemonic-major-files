from enum import Enum

class PegType(Enum):
    NOUN_TYPE = 0,
    ADJECTIVE_TYPE = 1
    VERB_TYPE = 2
    OTHER_TYPE = 3

    @staticmethod
    def convert_str(s):
        if s == "nouns":
            return PegType.NOUN_TYPE
        elif s == "adjectives":
            return PegType.ADJECTIVE_TYPE
        elif s == "verbs":
            return PegType.VERB_TYPE
        else:
            return PegType.OTHER_TYPE

class PegSheet:
    def __init__(self, fileStr):
        # Type dict is a dictionary of dictonaries which contain the pegs orderd
        # from 0-9, then 00-99
        self.typeDict = {}
        self.parse_file(fileStr)

    def __str__(self):
        return str(self.typeDict)

    def __repr__(self):
        return str(self.typeDict)

    def parse_file(self, fileStr):
        fo = open(fileStr)

        line = fo.readline()
        while line:
            if line.isspace() or line[0] == '#':
                line = fo.readline()
                continue

            if line[0] == '@':
                name = line[1:]
                name = name.rstrip("\n\r")
                
                list_type = PegType.convert_str(name)

                line = fo.readline()
                wordList = []
                while line and not line.isspace() and line[0] != '#' and line[0] != '@':
                    words = line.split(" ")
                    for w in words:
                        word = w.rstrip("\n\r")
                        wordList.append(word)
                    line = fo.readline()


                self.typeDict[list_type] = wordList

            line = fo.readline()

    def get_peg(self, numStr, list_type = PegType.NOUN_TYPE):
        if len(numStr) == 1:
            index = int(numStr)
        elif len(numStr) == 2:
            SINGLE_PEG_OFFSET = 10
            index = SINGLE_PEG_OFFSET + int(numStr)
        else:
            raise ValueError('A value outside the digit range of 1-2 was given.')
        return self.typeDict[list_type][index]
            