# First argument is the filename for batch file
# Second argument is the specific peg_count, if any

import phonetics
from collections import OrderedDict 

def metaphoneToMajor(input):

    mp_conv_dict = {
        '0' : '1',# th
        'A' : None,
        'B' : '9',
        'F' : '8',
        'H' : None,
        'J' : '6',
        'K' : '7',
        'L' : '5',
        'M' : '3',
        'N' : '2',
        'P' : '9',
        'R' : '4',
        'S' : '0',
        'T' : '1',
        'W' : None ,# sh and ch
        'X' : '6',
        'Y' : None,
    }

    out = ""
    for c in input:
        
        result = mp_conv_dict[c.encode('ascii', 'ignore')]
        if result:
            out += result

    return out

def fileToList(in_f):
    fi = open(in_f,"r")
    
    out_list = []

    line = fi.readline()
    while line:
        out_list.append(line.rstrip('\n\r'))
        line = fi.readline()

    fi.close()
    return out_list

def outputMajorDict(in_list, peg_count=-1):
    """Takes an in and out filename and uses key to sort. 
    Set specific peg counts only with peg_count.

    Parameters:
        in_list : list
        key : lambda
        peg_count : int
    """
    names_to_major = {}

    for e in in_list:
        metaphone_out = phonetics.dmetaphone(e)[0].encode('ascii', 'ignore')
        major_out = metaphoneToMajor(metaphone_out)

        if  peg_count == -1 or len(major_out) == peg_count:
            names_to_major[e] = (metaphone_out, major_out)
        
    return names_to_major

def outputMajortoFile(m_dict, out_f, skip_list, sort_key=None):
    fi = open(out_f,"w+")
    if sort_key:
        ordered = OrderedDict(sorted(
            m_dict.iteritems(), key = sort_key,
        ))

        for k, v in ordered.items():
            fi.write(k + "["  + v[0] + "]: " + v[1] + "\n")
    
    fi.write("\nSkipped:\n")
    for e in skip_list:
        fi.write("%s\n" % e)

    fi.close()
    return