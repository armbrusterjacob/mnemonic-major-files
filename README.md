## Various files relating to the Mnemonic Major system

### wordToMajor.py
Takes a list of words from file and converts them to their cooresponding metaphone output and from there into their Mnemonic Major version.
* First arguemnt is the filename
* Optional Arguments for:
    - Output file
    - Only display elements with specific peg count
    - Convert elements to singular version
    - Convert elements to plural version

### mnemonicMajor2Peg.py
Uses 2pegSheet.py, a list of nouns, verbs and adjectives that coorespond to 1-peg and  2-peg words sourced from [Wikipedia](https://en.wikipedia.org/wiki/Mnemonic_major_system).
This collection of words is the used to form adjective-noun-verb sequences based on the amount of pegs given by the user.

### mnemonicMajor.py
Simply a tool that naively outputs (usually) fake words of a user given length so that user can in turn guess the cooresponding number. Maybe useful for learning? probably not.