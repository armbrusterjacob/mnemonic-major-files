from random import randrange

syllableDict = {
    '0' : ('z', 's'),
    '1' : ('t', 'd'),
    '2' : ('n'),
    '3' : ('m'),
    '4' : ('r'),
    '5' : ('l'),
    '6' : ('ch'),
    '7' : ('k'),
    '8' : ('f'),
    '9' : ('p'),
}

fillerList = ('a', 'e', 'i', 'o', 'u')

continuing = True

while continuing:

    try:
        digits = int(raw_input("Enter the number of digits for the random number: "))
    except ValueError:
        print("Not an integer")
        continue
    try:
        chunks = int(raw_input("Chunks?: "))
    except ValueError:
        print("Not an integer")
        continue

    retrying = True
    while retrying: 
        randomNumber = randrange(pow(10, digits - 1), pow(10, digits) - 1)
        numStr = str(randomNumber)

        mnemonicStr = ""
        
        # Setup a dictionary so you don't reuse the same syllable in the same chunk if possible
        usedDict = {}

        for x in range(digits):
            if x != 0 and x % chunks == 0:
                mnemonicStr += " "

            mnemonic = syllableDict[numStr[x]]
            if type(mnemonic) == tuple:
                mnemonicStr += mnemonic[randrange(len(mnemonic))]
            else:
                mnemonicStr += str(mnemonic)

            if x != digits - 1 and (x == 0 or x % chunks != 0):
                mnemonicStr += str(fillerList[randrange(0, len(fillerList))])
        
        guessing = True
        while guessing:
            input = raw_input("What is \'%s\'?:" % mnemonicStr)

            if input.isdigit():
                if input == numStr:
                    print("Correct!")
                    guessing = False
                else:
                    print("Incorrect, try again.")
            elif input == "q":
                guessing = False
                retrying = False
                continuing = False
            elif input == 'c':
                print("The correct answer was \'%s\'." % numStr)
                guessing = False
                retrying = False
            elif input == 'g':
                print("The correct answer was \'%s\'." % numStr)
                guessing = False