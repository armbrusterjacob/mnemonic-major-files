from pattern.en import pluralize, singularize

def listToSingularList(in_list):
    out_list = []
    skip_list = []
    for e in in_list:

        try:
            out_list.append(singularize(e))
        except UnicodeDecodeError:
            skip_list.append(e)
    return (out_list, skip_list)

def listToPluralList(in_list):
    out_list = []
    skip_list = []
    for e in in_list:
        try:
            out_list.append(pluralize(e))
        except UnicodeDecodeError:
            skip_list.append(e)
    return (out_list, skip_list)
